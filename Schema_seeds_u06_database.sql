--SQLite3 Schema and seeds

BEGIN TRANSACTION;

PRAGMA foreign_keys=ON;

CREATE TABLE user_account (id INTEGER PRIMARY KEY AUTOINCREMENT,
                           email TEXT NOT NULL,
                           password TEXT NOT NULL,
                           first_name TEXT NOT NULL,
                           last_name TEXT NOT NULL,
                           user_level TEXT NOT NULL,
                           created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);

INSERT INTO user_account (email, password, first_name, last_name, user_level) VALUES('thebigcheese@gouda.gouda', 'storngpass', 'Geralt', 'of Rivia', 'admin');

CREATE TABLE sales_object (id INTEGER PRIMARY KEY AUTOINCREMENT,
                           street_adress TEXT NOT NULL,
                           city TEXT NOT NULL,
                           zip_code TEXT NOT NULL,
                           square_meter INTEGER NOT NULL,
                           price INTEGER NOT NULL);

INSERT INTO sales_object (street_adress, city, zip_code, square_meter, price) VALUES ('Toussaint st', 'Toussaint', '23456', 123, 23000000),
                                                                                     ('valley st', 'Kaer Morhen', '13373', 400, 35000000),
                                                                                     ('Crows perch st', 'Velen', '80085', 221, 5000000);

CREATE TABLE auction (id INTEGER PRIMARY KEY AUTOINCREMENT,
                      created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      end_date DATETIME NOT NULL,
                      starting_price INTEGER NOT NULL,
                      current_price INTEGER NOT NULL,
                      sales_object_id INTEGER NOT NULL,
                      FOREIGN KEY (sales_object_id) REFERENCES sales_object(id));

COMMIT;